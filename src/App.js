import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// Style
import "./styles.css";

// User Context
import UserContext from "./UserContext";

// Components
import AppNavbar from "./components/Navbar";
import AdminNavBar from "./components/adminNavBar";
import Footer from "./components/Footer";
import Error from "./pages/Error";
import UserLogin from "./pages/UserLogin";
import UserRegister from "./pages/UserRegister";
import ShowProducts from "./pages/ShowProducts";
import Home from "./pages/Home";
import UserDetails from "./pages/UserDetails";
import Settings from "./pages/Settings";
import OneProduct from "./pages/OneProduct";

import Cart from "./pages/Cart";
import Orders from "./pages/Orders";

export default function App() {
  const [user, setUser] = useState({ id: null, isAdmin: null });
  const unsetUser = () => {
    localStorage.clear();
    setUser({ id: null, isAdmin: null });
  };

  let token = localStorage.getItem("token");

  useEffect(() => {
    fetch("https://mighty-thicket-59478.herokuapp.com/users/me", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((details) => {
        if (details._id) {
          setUser({ id: details._id, isAdmin: details.isAdmin });
        } else {
          setUser({ id: null, isAdmin: null });
        }
      });
  }, [token]);

  let navBar = !user.isAdmin ? <AppNavbar /> : <AdminNavBar />;

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <BrowserRouter>
        {navBar}
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={UserLogin} />
          <Route exact path="/register" component={UserRegister} />
          <Route exact path="/products" component={ShowProducts} />
          <Route exact path="/products/:productId" component={OneProduct} />
          <Route exact path="/users/cart" component={Cart} />
          <Route exact path="/users/orders" component={Orders} />
          <Route exact path="/users/me" component={UserDetails} />
          <Route exact path="/users/me/settings" component={Settings} />
          <Route exact path="/admin/orders" component={Orders} />
          <Route exact path="*" component={Error} />
        </Switch>
        <Footer />
      </BrowserRouter>
    </UserContext.Provider>
  );
}
