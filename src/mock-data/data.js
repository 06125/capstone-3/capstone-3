const data = [
  {
    name: "Product 1",
    description: "Description 1",
    price: 1,
  },
  {
    name: "Product 2",
    description: "Description 2",
    price: 2,
  },
  {
    name: "Product 3",
    description: "Description 3",
    price: 3,
  },
  {
    name: "Product 4",
    description: "Description 4",
    price: 4,
  },
  {
    name: "Product 5",
    description: "Description 5",
    price: 5,
  },
  {
    name: "Product 6",
    description: "Description 6",
    price: 6,
  },
  {
    name: "Product 7",
    description: "Description 7",
    price: 7,
  },
  {
    name: "Product 8",
    description: "Description 8",
    price: 8,
  },
];

export default data;
