import React, { useContext } from "react";
import {
  Button,
  Container,
  Form,
  FormControl,
  Nav,
  Navbar,
} from "react-bootstrap";
import { NavLink, useHistory } from "react-router-dom";

import UserContext from "./../UserContext";

export default function AppNavbar() {
  const { unsetUser } = useContext(UserContext);
  const history = useHistory();

  const logout = () => {
    unsetUser();
    history.push("/login");
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={NavLink} to="/">
          gozzby
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/" className="mx-3 px-3">
              Admin
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="mx-3 px-3">
              Products
            </Nav.Link>
            <Nav.Link as={NavLink} to="/admin/orders" className="mx-3 px-3">
              Orders
            </Nav.Link>

            <Form className="d-flex mx-3 search-bar">
              <FormControl
                type="search"
                placeholder="Search"
                className="mr-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Nav>
          <Nav>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
