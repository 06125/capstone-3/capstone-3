import React from "react";
import { Card } from "react-bootstrap";

export default function Product({ product }) {
  let { _id, name, price, pictureLink } = product;
  if (name.length > 20) {
    name = name.split("").slice(0, 18).join("") + `...`;
  }

  let hreflink = `/products/${_id}`;

  return (
    <Card style={{ width: "18rem" }} className="my-3 col-3">
      <Card.Img variant="top" src={pictureLink} />
      <Card.Body>
        <Card.Title>
          <a href={hreflink}>{name}</a>
        </Card.Title>
        <Card.Text>Php {price}</Card.Text>
        <a href={hreflink} className="btn btn-primary">
          View Product
        </a>
      </Card.Body>
    </Card>
  );
}
