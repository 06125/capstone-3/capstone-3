import React, { Fragment, useContext } from "react";
import {
  Button,
  Container,
  Form,
  FormControl,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";
import { NavLink, useHistory } from "react-router-dom";

import UserContext from "./../UserContext";

export default function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext);
  const history = useHistory();

  const logout = () => {
    unsetUser();
    history.push("/login");
  };

  const leftSideNavBar = !user.id ? (
    <Fragment>
      <Nav.Link className="mx-3 px-3" as={NavLink} to="/login">
        Login
      </Nav.Link>
      <Nav.Link className="mx-3 px-3" as={NavLink} to="/register">
        Register
      </Nav.Link>
    </Fragment>
  ) : (
    <Fragment>
      {" "}
      <NavDropdown className="mx-3 px-3" title="Profile" id="categories">
        <NavDropdown.Item className="p-0 " as={NavLink} to="/users/cart">
          Cart
        </NavDropdown.Item>
        <NavDropdown.Item className="p-0" as={NavLink} to="/users/orders">
          Orders
        </NavDropdown.Item>
        <NavDropdown.Item className="p-0" as={NavLink} to="/users/me">
          Account
        </NavDropdown.Item>
        <NavDropdown.Item className="p-0" as={NavLink} to="/users/me/settings">
          Settings
        </NavDropdown.Item>
        <NavDropdown.Item className="p-0" onClick={logout}>
          Logout
        </NavDropdown.Item>
      </NavDropdown>
    </Fragment>
  );

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={NavLink} to="/">
          gozzby
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/" className="mx-3 px-3">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="mx-3 px-3">
              Products
            </Nav.Link>

            <Form className="d-flex mx-3 search-bar">
              <FormControl
                type="search"
                placeholder="Search"
                className="mr-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Nav>
          <Nav>{leftSideNavBar}</Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
