import React from "react";
import { Col, Row } from "react-bootstrap";

export default function CartItem({ item }) {
  let { _id, name, price, pictureLink } = item;
  let hreflink = `/products/${_id}`;

  return (
    <Row className="justify-content-center mb-3 cart-item">
      <Col xs={1} lg={1} className="cart-item__info">
        <img src={pictureLink} alt="" />
      </Col>
      <Col xs={7} lg={4} className="cart-item__info">
        <h2>
          <a href={hreflink}>{name}</a>
        </h2>
      </Col>
      <Col xs={4} lg={1} className="cart-item__info">
        <p className="price">Php {price.toFixed(2)}</p>
      </Col>
    </Row>
  );
}
