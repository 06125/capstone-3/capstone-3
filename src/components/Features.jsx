import React from "react";
import { Container } from "react-bootstrap";

export default function Features() {
  return (
    <Container fluid className="features">
      <div className="row justify-content-center p-5">
        <div className="col-2 d-flex flex-column align-items-center">
          <img src="/images/free-shipping.png" alt="" />

          <h1 className="features-title text-center">Free Shipping</h1>
          <p className="features-text">
            Over 100 free-shipping vouchers everyday. Shop Now.
          </p>
        </div>
        <div className="col-2 d-flex flex-column align-items-center">
          <img src="/images/money-guarantee.png" alt="" />

          <h1 className="features-title text-center">Money Guarantee</h1>
          <p className="features-text">
            Defective Product? Report to our customer care service to process
            your refund.
          </p>
        </div>
        <div className="col-2 d-flex flex-column align-items-center">
          <img src="/images/credit-card.png" alt="" />

          <h1 className="features-title text-center">Secure Payment</h1>
          <p className="features-text">
            Safer, faster, and more secure payment method
          </p>
        </div>
        <div className="col-2 d-flex flex-column align-items-center">
          <img src="/images/24-7.png" alt="" />

          <h1 className="features-title text-center">24/7 Support</h1>
          <p className="features-text">
            Customer care service ready to assist you 24/7.
          </p>
        </div>
      </div>
    </Container>
  );
}
