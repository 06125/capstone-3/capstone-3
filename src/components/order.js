import React, { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";

export default function Order({ ordersData }) {
  const { user } = useContext(UserContext);

  const [listItems, setListItems] = useState([]);
  let { _id, totalAmount, products, buyer } = ordersData;

  const purchasedOn = new Date(ordersData.purchasedOn).toString().split(" ");
  const date = purchasedOn.slice(1, 4).join(" ");
  const orderTime = purchasedOn[4];
  const buyerName = `${buyer.firstName} ${buyer.lastName}`;

  useEffect(() => {
    const itemArr = products.map((item) => {
      return (
        <li key={item._id}>
          {item.name}, Php {item.price}
        </li>
      );
    });

    setListItems(itemArr);
  }, [products]);

  return user.isAdmin ? (
    <tr>
      <td className="px-2 order-id">{_id}</td>
      <td className="px-2 order-buyer">
        <span>Name: </span>
        {buyerName}
        <br />
        <span>User ID: </span>
        {buyer._id}
      </td>
      <td className="text-center order-date">
        {date} {orderTime}
      </td>
      <td className="order-items">
        <ul className="m-0">{listItems}</ul>
        <p>{products.length} product(s)</p>
      </td>
      <td className="order-amount">Php {totalAmount.toFixed(2)}</td>
    </tr>
  ) : (
    <tr>
      <td className="px-2 order-id">{_id}</td>
      <td className="order-date">
        {date} {orderTime}
      </td>
      <td className="order-items">
        <ul className="m-0">{listItems}</ul>
        <p>{products.length} product(s)</p>
      </td>
      <td className="order-amount">Php {totalAmount.toFixed(2)}</td>
    </tr>
  );
}
