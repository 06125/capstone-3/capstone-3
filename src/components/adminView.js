import React, { useState, useEffect, Fragment } from "react";
import { Container, Table, Button, Modal, Form } from "react-bootstrap";

import Swal from "sweetalert2";

export default function AdminView({ productsData, fetchData }) {
  const [productId, setProductId] = useState("");
  const [products, setProducts] = useState([]);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [pictureLink, setPictureLink] = useState("");
  const [userId, setUserId] = useState("");

  const [showEdit, setShowEdit] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showAddAdmin, setShowAddAdmin] = useState(false);

  let token = localStorage.getItem("token");

  const openAddProduct = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);
  const openAddAdmin = () => setShowAddAdmin(true);
  const closeAddAdmin = () => setShowAddAdmin(false);

  const openEdit = (productId) => {
    fetch(`https://mighty-thicket-59478.herokuapp.com/products/${productId}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        let { _id, name, description, price, pictureLink } = result;
        setProductId(_id);
        setName(name);
        setDescription(description);
        setPrice(price);
        setPictureLink(pictureLink);
      });

    setShowEdit(true);
  };

  const closeEdit = () => {
    setShowEdit(false);
    setName("");
    setDescription("");
    setPrice(0);
  };

  useEffect(() => {
    const productArr = productsData.map((product) => {
      return (
        <tr key={product._id}>
          <td className="product-name">{product.name}</td>
          <td className="product-description">{product.description}</td>
          <td className="product-price">Php {product.price.toFixed(2)}</td>
          <td className="product-availability">
            {product.isActive ? (
              <span className="available">Available</span>
            ) : (
              <span className="unavailable">Unavailable</span>
            )}
          </td>
          <td className="product-btn">
            <Fragment>
              <Button
                className="mx-2 my-3"
                variant="primary"
                size="s"
                onClick={() => openEdit(product._id)}
              >
                Update
              </Button>
              <Button
                className="mx-1 my-3"
                variant="danger"
                size="s"
                onClick={() => deleteToggle(product._id)}
              >
                Delete
              </Button>
            </Fragment>

            {product.isActive === true ? (
              <Button
                className="mx-1 my-3"
                variant="warning"
                size="s"
                onClick={() => archiveToggle(product._id, product.isActive)}
              >
                Disable
              </Button>
            ) : (
              <Button
                className="mx-1 my-3"
                variant="success"
                size="s"
                onClick={() => unarchiveToggle(product._id, product.isActive)}
              >
                Enable
              </Button>
            )}
          </td>
        </tr>
      );
    });

    setProducts(productArr);
  }, [productsData]);

  /*edit product function*/
  const editProduct = (e, productId) => {
    e.preventDefault();

    fetch(
      `https://mighty-thicket-59478.herokuapp.com/admin/products/${productId}/edit`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          name,
          description,
          price,
          pictureLink,
        }),
      }
    )
      .then((result) => result.json())
      .then((result) => {
        fetchData();

        if (typeof result !== "undefined") {
          // alert("success")

          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product updated successfully!",
          });

          closeEdit();
        } else {
          fetchData();

          Swal.fire({
            title: "Failed",
            icon: "error",
            text: "Something went wrong!",
          });
        }
      });
  };

  /*Archive Product*/
  const archiveToggle = (productId, isActive) => {
    fetch(
      `https://mighty-thicket-59478.herokuapp.com/admin/products/${productId}/archive`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          isActive,
        }),
      }
    )
      .then((result) => result.json())
      .then((result) => {
        fetchData();
        if (result === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product successfully archived/unarchived",
          });
        } else {
          fetchData();
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  const unarchiveToggle = (productId, isActive) => {
    fetch(
      `https://mighty-thicket-59478.herokuapp.com/admin/products/${productId}/unarchive`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          isActive,
        }),
      }
    )
      .then((result) => result.json())
      .then((result) => {
        fetchData();
        if (result === true) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product successfully archived/unarchived",
          });
        } else {
          fetchData();
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  const deleteToggle = (productId) => {
    fetch(
      `https://mighty-thicket-59478.herokuapp.com/admin/products/${productId}/delete`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((result) => result.json())
      .then((result) => {
        fetchData();
        if (result) {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Deleted successfully",
          });
        } else {
          fetchData();
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  const addProduct = (e) => {
    e.preventDefault();
    fetch("https://mighty-thicket-59478.herokuapp.com/admin/products/add", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
      }),
    })
      .then((result) => result.json())
      .then((result) => {
        if (result === true) {
          fetchData();

          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product successfully added",
          });

          setName("");
          setDescription("");
          setPrice(0);

          closeAdd();
        } else {
          fetchData();

          Swal.fire({
            title: "Failed",
            icon: "error",
            text: "Something went wrong",
          });
        }
      });
  };

  const addAdmin = (e) => {
    e.preventDefault();
    fetch(
      `https://mighty-thicket-59478.herokuapp.com/admin/${userId}/set-admin`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          name,
          description,
          price,
        }),
      }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          fetchData();

          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Admin added",
          });

          setUserId("");

          closeAddAdmin();
        } else {
          fetchData();

          Swal.fire({
            title: "Failed",
            icon: "error",
            text: "Something went wrong",
          });
        }
      });
  };

  return (
    <Container className="admin-view">
      <div className="row justify-content-evenly pt-5">
        <h1 className="text-center">Admin Dashboard</h1>
        <div className="d-flex justify-content-end mb-3">
          <Button
            variant="primary"
            onClick={openAddProduct}
            className="mx-3 add-product"
          >
            Add New Product
          </Button>
          <Button
            variant="success"
            onClick={openAddAdmin}
            className="add-admin"
          >
            Add Admin
          </Button>
        </div>
      </div>
      <Table bordered hover>
        <thead>
          <tr>
            <th style={{ width: "12.5%" }} className="text-center">
              Name
            </th>
            <th style={{ width: "47%" }} className="text-center">
              Description
            </th>
            <th style={{ width: "8.5%" }} className="text-center">
              Price
            </th>
            <th style={{ width: "10%" }} className="text-center">
              Availability
            </th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          {/*display the products*/}
          {products}
        </tbody>
      </Table>

      {/*Edit Products Modal*/}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e, productId)}>
          <Modal.Header>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group productid="productLink">
              <Form.Label>Image Link</Form.Label>
              <Form.Control
                type="string"
                value={pictureLink}
                onChange={(e) => setPictureLink(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

      {/*Add Product Modal*/}
      <Modal show={showAdd} onHide={closeAdd}>
        <Form onSubmit={(e) => addProduct(e)}>
          <Modal.Header>Add Product</Modal.Header>
          <Modal.Body>
            <Form.Group productid="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group productid="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group productid="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group productid="productLink">
              <Form.Label>Image Link</Form.Label>
              <Form.Control
                type="string"
                value={pictureLink}
                onChange={(e) => setPictureLink(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeAdd}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

      {/*Make someone admin*/}
      <Modal show={showAddAdmin} onHide={closeAddAdmin}>
        <Form onSubmit={(e) => addAdmin(e)}>
          <Modal.Header>Add Admin</Modal.Header>
          <Modal.Body>
            <Form.Group productid="productName">
              <Form.Label>User ID</Form.Label>
              <Form.Control
                type="text"
                value={userId}
                onChange={(e) => setUserId(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeAddAdmin}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </Container>
  );
}
