import React from "react";
import { Container } from "react-bootstrap";

export default function Footer() {
  return (
    <Container fluid className="footer pb-1 ">
      <div className="row justify-content-center mt-5">
        <div className="col-6  mx-2">
          <h1 className="brand">gozzby</h1>
          <p className="footer-text">
            <span className="p-brand"> gozzby</span> is a global online store
            based in the Philippines. We sell different varieties of products,
            from clothings to gadgets. We always thrive to become the world's
            number one e-commerce.
          </p>
          <div className="footer-contact-info mb-2">
            <p className="contact-info-title mb-1">Contact Info</p>
            <p className="contact-info-text m-0">somePlaceThere, Philippines</p>
            <p className="contact-info-text m-0">
              (+12 ) 3456 7890, (+12 ) 0987 6543
            </p>
          </div>
          <div className="footer-social-media ">
            <a target="_blank" rel="noreferrer" href="/facebook.com">
              <img src="./images/facebook.png" alt="" />
            </a>
            <a target="_blank" rel="noreferrer" href="twitter.com">
              <img src="./images/twitter.png" alt="" />
            </a>
          </div>
        </div>
        <div className="col-2 mx-1">
          <div className="footer-title">Social Media</div>
          <p className="text-center">
            <a href="gozzby.com">Visit Our Webpage</a>
          </p>
        </div>
        <div className="col-1 mx-1">
          <div className="footer-title">Brands</div>
          <p className="text-center m-0 footer-text">Bench</p>
          <p className="text-center m-0 footer-text">H & M</p>
          <p className="text-center m-0 footer-text">Gucci</p>
          <p className="text-center m-0 footer-text">Asus</p>
        </div>
        <div className="col-1 mx-1">
          <div className="footer-title">Account</div>

          <p className="text-center m-0 footer-text">
            <a href="gozzby.com/newsletter">Newsletter</a>
          </p>
          <p className="text-center m-0 footer-text">
            <a href="gozzby.com/blogs">Blog</a>
          </p>
        </div>
      </div>
    </Container>
  );
}
