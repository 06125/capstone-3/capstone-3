import { Button, Container, Form } from "react-bootstrap";
import React from "react";
import { Link } from "react-router-dom";

export default function Banner() {
  return (
    <Container>
      <div className="container-fluid banner">
        <div className="row">
          <div className="col banner-brand">gozzby</div>
        </div>
        <div className="row ">
          <div className="col banner-text">Shop anywhere, everywhere.</div>
        </div>
        <Form className="banner-form">
          <Button className="banner-btn" as={Link} to="/products">
            Shop Now
          </Button>
        </Form>
      </div>
    </Container>
  );
}
