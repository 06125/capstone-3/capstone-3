import React, { useState, useEffect } from "react";

import { Container } from "react-bootstrap";

import Product from "./Product";

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    let productArr = productsData.map((prod) => {
      return <Product key={prod._id} product={prod} />;
    });
    setProducts(productArr);
  }, [productsData]);

  return (
    <Container className="show-products">
      <div className="row justify-content-evenly pt-5">
        <h1 className=" text-center">Products</h1>

        {/*display products*/}
        {products}
      </div>
    </Container>
  );
}
