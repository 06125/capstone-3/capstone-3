import React from "react";
import { Container } from "react-bootstrap";

export default function Blog() {
  return (
    <Container fluid className="blogs p-5">
      <div className="row mb-3">
        <div className="col-12 ">
          <h1 className="text-center -blogs">Blogs</h1>
        </div>
      </div>
      <div className="row blog-list mt-5 justify-content-center">
        <div className="col-12 col-md-5 blog-1 mx-3 d-flex flex-column justify-content-end p-5 pb-3">
          <h2 className="blog-title-main mb-4">
            Smart Child Safety Ideas Help Protect Babies, Toddlers{" "}
            <a
              target="_blank"
              rel="noreferrer"
              href="https://www.copyrightfreecontent.com/family/smart-child-safety-ideas-help-protect-babies-toddlers/"
            >
              Read More
            </a>
          </h2>
        </div>
        <div className="col-12 col-md-6 mx-3">
          <div className="row d-flex justify-content-evenly">
            <div className="col-5 p-4 blog-2 d-flex flex-column justify-content-end">
              <h2 className="blog-title mb-2">
                Chemical-Free Air Purifier Protects Public Spaces <br />
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.copyrightfreecontent.com/health/chemical-free-air-purifier-protects-public-spaces/"
                >
                  Read More
                </a>
              </h2>
            </div>
            <div className="col-6 p-4 blog-3 d-flex flex-column justify-content-end ">
              <h2 className="blog-title mb-2">
                Yale OB/GYN Offers Helpful Tips to Prep for Pregnancy{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.copyrightfreecontent.com/health/yale-ob-gyn-offers-helpful-tips-to-prep-for-pregnancy/"
                >
                  Read More
                </a>
              </h2>
            </div>
          </div>
          <div className="row d-flex justify-content-evenly my-3">
            <div className="col-6 p-4 blog-4 d-flex flex-column justify-content-end">
              <h2 className="blog-title mb-2">
                New Subway Sandwiches Reinvent the Grilled Cheese{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.copyrightfreecontent.com/food/new-subway-sandwiches-reinvent-the-grilled-cheese/"
                >
                  Read More
                </a>
              </h2>
            </div>
            <div className="col-5 p-4 blog-5 d-flex flex-column justify-content-end">
              <h2 className="blog-title mb-2 ">
                Healthy Eating Adds Up to a Healthy Heart{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.copyrightfreecontent.com/food/healthy-eating-adds-up-to-a-healthy-heart/"
                >
                  Read More
                </a>
              </h2>
            </div>
          </div>
        </div>
      </div>

      {/*  <div className="row mt-3">
        <Form>
          <Button>View All Blogs</Button>
        </Form>
        <div className="col-12 ">
          <h1 className="text-center -blogs">Blogs</h1>
        </div>
      </div> */}
    </Container>
  );
}
