import React from "react";
import { Container } from "react-bootstrap";

export default function Error() {
  return (
    <Container className="error-404 p-4 d-flex flex-column justify-content-center">
      <div className="row ">
        <div className="col"></div>
      </div>
      <div className="row">
        <div className="col ">
          <h1 className="text-center">Error 404</h1>
          <p className="text-center">
            Our developer, Emil, made a huge mistake.
          </p>
          <p className="text-center">Should we fire him?</p>
        </div>
      </div>
      <img src="" alt="" />
    </Container>
  );
}
