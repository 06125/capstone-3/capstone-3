import React, { useContext, useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { Redirect } from "react-router";
import Swal from "sweetalert2";

import UserContext from "./../UserContext";

export default function UserLogin() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isDisabled, setIsDisabled] = useState(true);

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (email && password) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password]);

  const login = (e) => {
    e.preventDefault();

    fetch("https://mighty-thicket-59478.herokuapp.com/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    })
      .then((result) => result.json())
      .then((token) => {
        if (token) {
          localStorage.setItem("token", token.access);
          userDetails(token.access);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
          });

          setEmail("");
          setPassword("");
          return;
        }

        Swal.fire({
          title: "Something Went Wrong",
          icon: "error",
          text: "Email or Password Incorrect",
        });
        return;
      });

    const userDetails = (token) => {
      fetch("https://mighty-thicket-59478.herokuapp.com/users/me", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((result) => result.json())
        .then((user) => {
          setUser({ id: user._id, isAdmin: user.isAdmin });
        });
    };
  };

  return user.id ? (
    <Redirect to="/"></Redirect>
  ) : (
    <Container className="user-login my-5 d-flex justify-content-center flex-column">
      <div className="row justify-content-center">
        <div className="col-12 col-md-5 col-lg-4">
          <h1 className=" text-center">Sign In</h1>
          <Form className="form" onSubmit={(e) => login(e)}>
            <Form.Group className="mb-3 " controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={isDisabled}>
              Submit
            </Button>
          </Form>
        </div>
      </div>
    </Container>
  );
}
