import React, { useState } from "react";
import { Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";

import { useParams } from "react-router-dom";

import Swal from "sweetalert2";

export default function OneProduct() {
  const [product, setProduct] = useState({});
  const token = localStorage.getItem("token");

  const { productId } = useParams();

  fetch(`https://mighty-thicket-59478.herokuapp.com/products/${productId}/`, {
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((result) => result.json())
    .then((result) => {
      if (result) {
        setProduct(result);
        return;
      }
    });

  let { name, price, description, pictureLink } = product;

  const archive = (e) => {
    e.preventDefault();
    return fetch(
      `https://mighty-thicket-59478.herokuapp.com/products/${productId}/archive`,
      { method: "put", headers: { Authorization: `Bearer ${token}` } }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          return Swal.fire({
            title: "Product Archived Successfully.",
            icon: "success",
          });
        }
      });
  };

  const unarchive = (e) => {
    e.preventDefault();

    return fetch(
      `https://mighty-thicket-59478.herokuapp.com/products/${productId}/unarchive`,
      { method: "put", headers: { Authorization: `Bearer ${token}` } }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          return Swal.fire({
            title: "Products Unarchive Successfully.",
            icon: "success",
          });
        }
      });
  };

  const Delete = (e) => {
    e.preventDefault();
    return fetch(
      `https://mighty-thicket-59478.herokuapp.com/products/${productId}/delete`,
      { method: "put", headers: { Authorization: `Bearer ${token}` } }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          return Swal.fire({
            title: "Product Deleted Successfully.",
            icon: "success",
          });
        }
      });
  };

  return name ? (
    <Container fluid className="one-product">
      <div className="row px-0">
        <div className="col px-0">
          <img
            className="banner"
            src="https://i.postimg.cc/66R98LvV/dan-meyers-4-K6jg-Kr3-Ox-Y-unsplash.jpg"
            alt=""
          />
        </div>
      </div>
      <Row className="justify-content-center pt-5 one-product-proper">
        <Col lg={5}>
          <img src={pictureLink} alt="" />
        </Col>
        <Col lg={4} className="d-flex mx-3 flex-column justify-content-center">
          <h1 className="mb-3 title">{name}</h1>
          <h2 className="mb-3 price text-muted">Php {price}</h2>
          <p className="mb-5 description text-justify">{description}</p>
          <div>
            <Form onSubmit={archive} className="d-inline-block">
              <Button variant="primary" type="submit">
                Archive
              </Button>
            </Form>
            <Form onSubmit={unarchive} className="d-inline-block">
              <Button className="mx-3" variant="primary" type="submit">
                Unarchive
              </Button>
            </Form>
            <Form onSubmit={Delete} className="d-inline-block">
              <Button variant="primary" type="submit">
                Delete
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  ) : (
    <Container fluid className="one-product">
      <div className="row px-0">
        <div className="col px-0">
          <img
            className="banner"
            src="https://i.postimg.cc/66R98LvV/dan-meyers-4-K6jg-Kr3-Ox-Y-unsplash.jpg"
            alt=""
          />
        </div>
      </div>
      <Row className="justify-content-center pt-5 one-product-proper">
        <Spinner animation="border" role="status" className="mt-5">
          <span className="sr-only"></span>
        </Spinner>
      </Row>
    </Container>
  );
}
