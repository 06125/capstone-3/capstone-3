import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import Swal from "sweetalert2";

import CartItem from "../components/CartItem";

export default function Cart() {
  const [cartItems, setCartItems] = useState([]);
  const [itemData, setItemData] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  const token = localStorage.getItem("token");

  fetch("https://mighty-thicket-59478.herokuapp.com/users/cart/", {
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((result) => result.json())
    .then((result) => setCartItems(result));

  useEffect(() => {
    let itemArr = cartItems.map((item) => {
      return <CartItem key={item._id} item={item} />;
    });

    setItemData(itemArr);
  }, [cartItems]);

  useEffect(() => {
    let price = 0;

    cartItems.forEach((item) => (price += item.price));
    setTotalPrice(price.toFixed(2));
  }, [cartItems]);

  const buyAll = () => {
    fetch("https://mighty-thicket-59478.herokuapp.com/users/cart/check-out", {
      method: "post",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        console.log(result);
        if (result) {
          return Swal.fire({
            title: "Order Created Successfully.",
            icon: "success",
            text: "Visit order page for more details",
          });
        }
      });
  };

  return cartItems[0] ? (
    <Container className="show-cart-items mb-3">
      <Row className="mb-3">
        <Col>
          <div className="row justify-content-evenly pt-5">
            <h1 className=" text-center">Cart Items</h1>
          </div>
        </Col>
      </Row>

      {/* Display cart items */}
      {itemData}

      <Row className="justify-content-center">
        <Col xs={8} lg={5}>
          <p className="total-price__text">Total Price:</p>
        </Col>

        <Col xs={4} lg={1} className="total-price">
          <p>Php {totalPrice}</p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col xs={8} lg={6} className="buy-all d-flex justify-content-end">
          <Button className="buy-all__btn" onClick={buyAll}>
            Buy All
          </Button>
        </Col>
      </Row>
    </Container>
  ) : (
    <Container className="show-cart-items mb-3">
      <Row>
        <Col>
          <div className="row justify-content-evenly pt-5">
            <h1 className="text-center mb-2">Cart Items</h1>
            <h2 className="text-center">Cart Empty</h2>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
