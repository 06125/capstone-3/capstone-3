import React, { useEffect, useState } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";

export default function UserDetail() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const token = localStorage.getItem("token");

  useEffect(() => {
    fetch("https://mighty-thicket-59478.herokuapp.com/users/me", {
      method: "get",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        let { firstName, lastName, email } = result;
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
      });
  }, [token]);

  return (
    <Container className="user-details">
      <div className="row pt-5">
        <h1 className="text-center">{firstName}'s Details</h1>
        <Form>
          <Form.Group
            as={Row}
            className="mb-3 justify-content-center"
            controlId="firstName"
          >
            <Form.Label column sm="1" lg="2">
              First Name
            </Form.Label>
            <Col sm="10" md={4}>
              <Form.Control plaintext readOnly defaultValue={firstName} />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3 justify-content-center"
            controlId="email"
          >
            <Form.Label column sm="1" lg="2">
              Last Name
            </Form.Label>
            <Col sm="10" md={4}>
              <Form.Control plaintext readOnly defaultValue={lastName} />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3 justify-content-center"
            controlId="email"
          >
            <Form.Label column sm="1" lg="2">
              Email
            </Form.Label>
            <Col sm="10" md={4}>
              <Form.Control plaintext readOnly defaultValue={email} />
            </Col>
          </Form.Group>
        </Form>
      </div>
    </Container>
  );
}
