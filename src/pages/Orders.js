import React, { useContext, useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap";
import Order from "../components/order";
import UserContext from "../UserContext";

export default function Orders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const [orderData, setOrderData] = useState([]);

  const token = localStorage.getItem("token");

  useEffect(() => {
    fetchOrders();
  }, [user]);

  const fetchOrders = () => {
    const endpoint = user.isAdmin ? "admin/orders" : "users/orders";

    fetch(`https://mighty-thicket-59478.herokuapp.com/${endpoint}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        setOrders(result);
      });
  };

  useEffect(() => {
    const orderArr = orders.map((item) => {
      return <Order key={item._id} ordersData={item} />;
    });

    setOrderData(orderArr);
  }, [token, orders]);

  const tableHead = user.isAdmin ? (
    <tr>
      <th>Order Id</th>
      <th>Buyer</th>
      <th>Purchased On</th>
      <th>Items</th>
      <th>Amount</th>
    </tr>
  ) : (
    <tr>
      <th>Order Id</th>

      <th>Purchased On</th>
      <th>Items</th>
      <th>Amount</th>
    </tr>
  );

  return (
    <Container className="p-5 orders">
      <h1 className="text-center">ORDERS</h1>
      <Table bordered hover>
        <thead className="text-center">{tableHead}</thead>
        <tbody>{orderData}</tbody>
      </Table>
    </Container>
  );
}
