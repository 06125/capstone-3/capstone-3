import React, { useEffect, useState } from "react";
import { Container, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Settings() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const token = localStorage.getItem("token");
  const history = useHistory();

  // Getting the details
  useEffect(() => {
    fetch("https://mighty-thicket-59478.herokuapp.com/users/me", {
      method: "get",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then(({ firstName, lastName, email }) => {
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
      });
  }, [token]);

  const editDetails = (e) => {
    e.preventDefault();
    fetch("https://mighty-thicket-59478.herokuapp.com/users/me/settings", {
      method: "Put",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
      }),
    })
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Details Updated Successfully",
            icon: "success",
          });
        }
      });

    history.push("/login");
  };

  return (
    <Container className="settings">
      <div className="row pt-5 justify-content-center">
        <h1 className="text-center">Edit Details</h1>

        <Form className="form col-8 col-md-4" onSubmit={(e) => editDetails(e)}>
          <Form.Group className="mb-3 " controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter First Name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3 " controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Last Name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3 " controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>

          <Button variant="primary" type="submit">
            Edit
          </Button>
        </Form>
      </div>
    </Container>
  );
}
