import React, { useContext, useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "./../UserContext";

export default function UserRegister() {
  let history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [verifyPassword, setVerifyPassword] = useState("");
  const [isDisabled, setIsDisabled] = useState(true);

  const { user } = useContext(UserContext);

  useEffect(() => {
    if (
      firstName &&
      lastName &&
      email &&
      password &&
      verifyPassword &&
      password === verifyPassword
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [firstName, lastName, email, password, verifyPassword]);

  const register = (e) => {
    e.preventDefault();

    fetch(
      "https://mighty-thicket-59478.herokuapp.com/register/check-credentials",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      }
    )
      .then((result) => result.json())
      .then((result) => {
        if (!result) {
          Swal.fire({
            title: "Email Already Used.",
            icon: "error",
            text: "Enter Another Email",
          });
        } else {
          return fetch("https://mighty-thicket-59478.herokuapp.com/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName,
              lastName,
              email,
              password,
            }),
          });
        }
      })
      .then((result) => result?.json())
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Registration Successful",
            icon: "success",
          });

          history.push("/login");
        } else {
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: "Please Try Again",
          });
        }
      });
  };

  return user.id ? (
    <Redirect to="/"></Redirect>
  ) : (
    <Container className="user-login my-5 d-flex justify-content-center flex-column">
      <div className="row justify-content-center">
        <div className="col-12 col-md-5 col-lg-4">
          <h1 className=" text-center">Register</h1>
          <Form className="form" onSubmit={(e) => register(e)}>
            <Form.Group className="mb-3 " controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter First Name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3 " controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3 " controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="verifyPassword">
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Verify Password"
                value={verifyPassword}
                onChange={(e) => setVerifyPassword(e.target.value)}
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={isDisabled}>
              Submit
            </Button>
          </Form>
        </div>
      </div>
    </Container>
  );
}
