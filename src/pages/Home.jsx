import React from "react";
import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import Blog from "../components/Blog";
import Features from "../components/Features";

export default function Home() {
  return (
    <Container fluid className="p-0">
      <Banner />
      <Features />
      <Blog />
    </Container>
  );
}
