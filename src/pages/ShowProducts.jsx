import React, { useContext, useEffect, useState } from "react";
import { Container, Spinner } from "react-bootstrap";

import UserContext from "./../UserContext";
import UserView from "../components/userView";
import AdminView from "../components/adminView";

export default function ShowProducts() {
  const [productsData, setProductsData] = useState([]);

  const token = localStorage.getItem("token");
  const { user } = useContext(UserContext);

  const fetchData = () => {
    let path = user.isAdmin
      ? "https://mighty-thicket-59478.herokuapp.com/admin/products/all"
      : "https://mighty-thicket-59478.herokuapp.com/products";

    if (user.id) {
      fetch(path, {
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((result) => result.json())
        .then((result) => {
          setProductsData(result);
        });
    }
  };

  useEffect(() => {
    fetchData();
  });

  return productsData[0] ? (
    !user.isAdmin ? (
      <UserView productsData={productsData} />
    ) : (
      <AdminView productsData={productsData} fetchData={fetchData} />
    )
  ) : (
    <Container className="show-products">
      <div className="row justify-content-evenly pt-5">
        <h1 className=" text-center">Products</h1>
        <Spinner animation="border" role="status" className="mt-5">
          <span className="sr-only"></span>
        </Spinner>
      </div>
    </Container>
  );
}
