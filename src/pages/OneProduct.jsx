import React, { useState } from "react";
import { Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";

import { useParams } from "react-router-dom";

import Swal from "sweetalert2";

export default function OneProduct() {
  const [product, setProduct] = useState({});
  const token = localStorage.getItem("token");

  const { productId } = useParams();

  fetch(`https://mighty-thicket-59478.herokuapp.com/products/${productId}/`, {
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((result) => result.json())
    .then((result) => {
      if (result) {
        setProduct(result);
        return;
      }
    });

  let { name, price, description, pictureLink } = product;

  const buyNow = (e) => {
    e.preventDefault();
    return fetch(
      `https://mighty-thicket-59478.herokuapp.com/products/${productId}/check-out`,
      { method: "post", headers: { Authorization: `Bearer ${token}` } }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          return Swal.fire({
            title: "Order Created Successfully.",
            icon: "success",
            text: "Visit order page for more details",
          });
        }
      });
  };

  const addToCart = (e) => {
    e.preventDefault();
    return fetch(
      `https://mighty-thicket-59478.herokuapp.com/products/${productId}/addToCart`,
      { method: "put", headers: { Authorization: `Bearer ${token}` } }
    )
      .then((result) => result.json())
      .then((result) => {
        if (result) {
          return Swal.fire({
            title: "Added to Cart Successfully.",
            icon: "success",
          });
        }
      });
  };

  return name ? (
    <Container fluid className="one-product">
      <div className="row px-0">
        <div className="col px-0">
          <img
            className="banner"
            src="https://i.postimg.cc/66R98LvV/dan-meyers-4-K6jg-Kr3-Ox-Y-unsplash.jpg"
            alt=""
          />
        </div>
      </div>
      <Row className="justify-content-center pt-5 one-product-proper">
        <Col lg={5}>
          <img src={pictureLink} alt="" />
        </Col>
        <Col lg={4} className="d-flex mx-3 flex-column justify-content-center">
          <h1 className="mb-3 title">{name}</h1>
          <h2 className="mb-3 price text-muted">Php {price}</h2>
          <p className="mb-5 description text-justify">{description}</p>
          <div>
            <Form onSubmit={buyNow} className="d-inline-block">
              <Button variant="primary" type="submit">
                Buy Now
              </Button>
            </Form>
            <Form onSubmit={addToCart} className="d-inline-block">
              <Button className="mx-3" variant="primary" type="submit">
                Add to Cart
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  ) : (
    <Container fluid className="one-product">
      <div className="row px-0">
        <div className="col px-0">
          <img
            className="banner"
            src="https://i.postimg.cc/66R98LvV/dan-meyers-4-K6jg-Kr3-Ox-Y-unsplash.jpg"
            alt=""
          />
        </div>
      </div>
      <Row className="justify-content-center pt-5 one-product-proper">
        <Spinner animation="border" role="status" className="mt-5">
          <span className="sr-only"></span>
        </Spinner>
      </Row>
    </Container>
  );
}
